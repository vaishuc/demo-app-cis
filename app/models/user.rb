class User < ApplicationRecord  
  has_many :blogs
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  def full_name
    "#{first_name} #{last_name}"          
  end

  def is_blog_author? blog
    self.id == blog.user_id
  end

end

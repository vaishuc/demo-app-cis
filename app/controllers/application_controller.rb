class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception	
	before_action :configure_permitted_parameters, :only => [:create], if: :devise_controller?

  def configure_permitted_parameters  	
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :password, :password_confirmation, :title_id, :province_id, :first_name, :last_name])    
  end   

	def after_sign_in_path_for(resource)
	  blogs_path
	end

	def after_sign_out_path_for resource
		blogs_path			
	end		
end

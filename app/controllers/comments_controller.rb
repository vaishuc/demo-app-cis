class CommentsController < ApplicationController
	before_action :authenticate_user!

	def new
   @blog = Blog.find(params[:blog_id])
   @comment = @blog.comments.new(parent_id: params[:parent_id])		
	end

  def create
  	# debugger
    @blog = Blog.find(params[:blog_id])
    json_params = comment_params.merge({user_id: current_user.id})
    @comment = @blog.comments.create(json_params)
    redirect_to blog_path(@blog)
  end	

  private
    def comment_params
      params.require(:comment).permit(:body, :parent_id)
    end  
end
